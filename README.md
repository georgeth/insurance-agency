Insurance Agent commission calculator
---

Example usage
```sh
node bin/index.js --agent-commission-file conf/agent_commission.yaml \
                  --agent-hierarchy-file conf/agent_hierarchy.yaml \
                  --commission-plan-file conf/commission_plan.yaml \
                  --policy-amount 100000 \
                  --hierarchy-name H1 \
                  --commission-plan A \
                  --agent-name AgentBob
```
