#!/usr/bin/env node
'use strict';

var program = require('commander'),
    yaml = require('js-yaml'),
    fs = require('fs'),
    commission = require('../lib/commission');

//TODO: Make some options mandatory, or set default values
program
    .version('0.1.0', '-v, --version')
    .option('--agent-commission-file <path>', 'Agent commission file')
    .option('--agent-hierarchy-file <path>', 'Agent hierarchy file')
    .option('--commission-plan-file <path>', 'Commission plan')
    .option('--policy-amount <amount>', '$ Policy amount')
    .option('--hierarchy-name <hierarchy>', 'Agenct Hierarchy rule')
    .option('--commission-plan <plan>', 'Commission plan')
    .option('--agent-name <agent>', 'Agent who sold the policy')
    .parse(process.argv);

var agentCommission = commission.readYAML(program.agentCommissionFile),
    agentHierarchy =  commission.readYAML(program.agentHierarchyFile),
    commissionPlans = commission.readYAML(program.commissionPlanFile);

var commissionPercent = commission.getCommissionSchedule(
    agentHierarchy[program.hierarchyName],
    commissionPlans[program.commissionPlan],
    agentCommission);

for(let agent in commissionPercent) {
    let commission = commissionPercent[agent] * program.policyAmount;
    console.log(agent + ': '  + commission.toFixed(2));
}
