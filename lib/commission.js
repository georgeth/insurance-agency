#!/usr/bin/env node
'use strict';

var yaml = require('js-yaml'),
    fs = require('fs');

exports.readYAML = function readYAML(file) {
    console.log('Reading ' + file);
    let yamlData = yaml.safeLoad(fs.readFileSync(file, 'utf8'));
    return yamlData;
};

exports.getCommissionSchedule = function getCommissionSchedule(hierarchy, plan, agentCommission) {
    let schedule = {};
    for(let iter = 0; iter < hierarchy.length && iter < plan.length; iter++) {
        let agentName = hierarchy[iter];
        schedule[agentName] = agentCommission[agentName] * plan[iter];
    }
    return schedule;
};
