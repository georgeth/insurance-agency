'use strict';
var assert = require('assert'),
    commission = require('../../lib/commission');

describe('commission schedule', function() {
    it('getCommissionSchedule one', function() {
        let hierarchy = ['AgentBob', 'SuperAgentX', 'SuperAgentA', 'SuperAgentC'],
            plan = [0.5, 0.05],
            agentCommission = {'AgentBob': 0.02,
                               'SuperAgentA':0.0325,
                               'SuperAgentC':0.0225,
                               'SuperAgentD':0.0275,
                               'SuperAgentX':0.025,
                               'SuperAgentZ':0.0175};
        let expected = {'AgentBob': 0.5 * 0.02, 'SuperAgentX': 0.05 * 0.025};

        let got = commission.getCommissionSchedule(hierarchy, plan, agentCommission);
        assert.deepEqual(got, expected)
    });
    it('getCommissionSchedule two', function() {
        let hierarchy = ['AgentBob', 'SuperAgentA', 'SuperAgentX', 'SuperAgentD', 'SuperAgentZ'],
            plan = [0.7, 0.08, 0.04],
            agentCommission = {'AgentBob': 0.02,
                               'SuperAgentA':0.0325,
                               'SuperAgentC':0.0225,
                               'SuperAgentD':0.0275,
                               'SuperAgentX':0.025,
                               'SuperAgentZ':0.0175};
        let expected = {'AgentBob': 0.7 * 0.02, 'SuperAgentA': 0.08 * 0.0325, 'SuperAgentX': 0.04 * 0.025};

        let got = commission.getCommissionSchedule(hierarchy, plan, agentCommission);
        assert.deepEqual(got, expected)
    });
});
